# basic
import traceback
import numpy as np
import pandas as pd

# get data
import pandas_datareader as pdr

# visual
import matplotlib.pyplot as plt
import mpl_finance as mpf
import seaborn as sns

#time
import datetime as datetime

#talib
import talib

if __name__ == "__main__":

	#Yahoo Finance
	#只提供1997/07/02以後的資料
	#提供的資料欄位: High, Low, Open, Close, Volume, Adj Close
	#台灣加權股價指數: ^TWII
	#個股查詢: 如中鋼 2002.TW

	#Google Finance
	#台灣加權股價指數只提供1999/03/24以後的資料
	#台灣加權股價指數: TPE:TAIEX
	#個股查詢: 如中鋼 2002.tw

    try:
        start = datetime.datetime(2019,10,1)
        df_2330 = pdr.DataReader('2330.TW', 'yahoo', start=start)

        #modify index from Datetime to string
        df_2330.index = df_2330.index.format(formatter=lambda x: x.strftime('%Y-%m-%d'))

        #print(df_2330.shape)
        #print(df_2330.columns)
        for index, data in df_2330.iterrows():
        	print(index)
        	print(data.Open, data.High, data.Low, data.Close)

        fig = plt.figure(figsize=(10, 5))
        #新增Figure的軸（左,下,寬度,高度)，範圍佔Figure的比例（數值介於0-1）
        ax = fig.add_axes([0.1,0.6,0.9,0.4])
        ax2 = fig.add_axes([0.1,0.4,0.9,0.2])
        ax3 = fig.add_axes([0.1,0.1,0.9,0.3])

        #k線圖
        ax.set_xticks(range(0, len(df_2330.index), 10))
        ax.set_xticklabels(df_2330.index[::10])
        mpf.candlestick2_ochl(ax, df_2330['Open'], df_2330['Close'], df_2330['High'],
                              df_2330['Low'], width=0.6, colorup='r', colordown='g', alpha=0.75);

        #均線
        sma_5 = talib.SMA(np.array(df_2330['Close']), 5)
        sma_10 = talib.SMA(np.array(df_2330['Close']), 10)
        plt.rcParams['font.sans-serif']=['Microsoft JhengHei']
        #plt.rcParams['font.sas-serig']=['SimHei'] #用來正常顯示中文標籤
        #plt.rcParams['axes.unicode_minus']=False #用來正常顯示負號
        ax.plot(sma_5, label='5日均線')
        ax.plot(sma_10, label='10日均線')

        #kd
        df_2330['k'], df_2330['d'] = talib.STOCH(df_2330['High'], df_2330['Low'], df_2330['Close'])
        df_2330['k'].fillna(value=0, inplace=True)
        df_2330['d'].fillna(value=0, inplace=True)
        ax2.plot(df_2330['k'], label='K值')
        ax2.plot(df_2330['d'], label='D值')
        ax2.set_xticks(range(0, len(df_2330.index), 10))
        ax2.set_xticklabels(df_2330.index[::10])

        #成交量
        mpf.volume_overlay(ax3, df_2330['Open'], df_2330['Close'], df_2330['Volume'], colorup='r', colordown='g', width=0.5, alpha=0.8)
        ax3.set_xticks(range(0, len(df_2330.index), 10))
        ax3.set_xticklabels(df_2330.index[::10])

        ax.legend()
        ax2.legend()
        plt.show()
    except:
    	print(traceback.format_exc())
